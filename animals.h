///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_02_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>
#define MAX_SPECIES (20)

enum Gender {MALE, FEMALE};
char* gendertype (enum Gender gender);

enum Breed {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
char* breedtype (enum Breed breed);

char* isfixed (bool isFixed);

enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};
char* colortype (enum Color color);
