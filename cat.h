///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_02_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>
#include "animals.h"

struct Cat{
   char name[30];
   enum Gender gender;
   enum Breed breed;
   bool isFixed;
   float weight;
   enum Color color1;
   enum Color color2;
   long license;
};

void addAliceTheCat(int i);

void printCat(int i);
