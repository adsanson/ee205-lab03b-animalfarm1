///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>
#include "animals.h"

char* gendertype (enum Gender gender){
switch(gender){
   case MALE:
      return("Male");
   case FEMALE:
      return("Female");
   }
      return 0;
}

char* breedtype (enum Breed breed){
switch(breed){
   case MAIN_COON:
      return("Main Coon");
   case MANX:
      return("Manx");
   case SHORTHAIR:
      return("Short Hair");
   case PERSIAN:
      return("Persian");
   case SPHYNX:
      return("Sphynx");
   }
      return 0;
}

char* isfixed (bool isFixed){
switch(isFixed){
   case true:
      return("Yes");
   case false:
      return("No");
   }
      return 0;
}

char* colortype (enum Color color) {
switch (color){
   case BLACK:
      return("Black");
   case WHITE:
      return("White");
   case RED:
      return("Red");
   case BLUE:
      return("Blue");
   case GREEN:
      return("Green");
   case PINK:
      return("Pink");
   }
     return 0;
}
