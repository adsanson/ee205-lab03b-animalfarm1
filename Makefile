###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Albert D'Sanson <adsanson@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   01_02_2021
###############################################################################

CC		 = gcc
CFLAGS = -g -Wall

all: animalfarm

main.o: main.c
			$(CC) $(CFLAGS) -c main.c

cat.o: cat.c cat.h
			$(CC) $(CFLAGS) -c cat.c

animals.o: animals.c animals.h
			$(CC) $(CFLAGS) -c animals.c

animalfarm: main.o animals.o cat.o
			$(CC) $(CFLAGS) -o animalfarm main.o animals.o cat.o

clean:
	rm -f *.o animalfarm
