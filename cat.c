///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   01_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "cat.h"

struct Cat catDB[MAX_SPECIES];

void addAliceTheCat(int i) {
strcpy(catDB[i].name, "Alice");
   catDB[i].gender = FEMALE;
   catDB[i].breed = MAIN_COON;
   catDB[i].isFixed = true;
   catDB[i].weight = 12.34;
   catDB[i].color1 = BLACK;
   catDB[i].color2 = RED;
   catDB[i].license = 12345;
}

void printCat(int i) {
   //print datainfo
   printf("Cat name = [%s]\n", catDB[i].name);
   printf("    gender = [%s]\n", gendertype(catDB[i].gender));
   printf("    breed = [%s]\n", breedtype(catDB[i].breed));
   printf("    isFixed = [%s]\n", isfixed(catDB[i].isFixed));
   printf("    weight = [%.2f]\n", catDB[i].weight);
   printf("    collar color 1 = [%s]\n", colortype(catDB[i].color1));
   printf("    collar color 2 = [%s]\n", colortype(catDB[i].color2));
   printf("    license = [%ld]\n",catDB[i].license);
}
